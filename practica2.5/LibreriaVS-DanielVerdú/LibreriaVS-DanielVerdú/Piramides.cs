﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_DanielVerdú
{
    public class Piramides
    {
        public int BasePiramide;
        public int AlturaPiramide;
        public Piramides(int basePiramide, int alturaPiramide)
        {
            this.BasePiramide = basePiramide;
            this.AlturaPiramide = alturaPiramide;
        }
        public void PiramideUno()
        {
            int basePiramide = BasePiramide;
            for (int i = 0; i < AlturaPiramide; i++)
            {
                for (int j = 0; j < basePiramide; j++)
                {
                    Console.Write("1");
                }
                basePiramide--;
                Console.WriteLine("");
            }
        }
        public void Piramidedos()
        {
            int basePiramide = BasePiramide;
            for (int i = 0; i < AlturaPiramide; i++)
            {
                for (int j = 0; j < basePiramide; j++)
                {
                    Console.Write("2");
                }
                basePiramide--;
                Console.WriteLine("");
            }
        }
      
        public void Piramidecinco()
        {
            int basePiramide = BasePiramide;
            for (int i = 0; i < AlturaPiramide; i++)
            {
                for (int j = 0; j < basePiramide; j++)
                {
                    Console.Write("5");
                }
                basePiramide--;
                Console.WriteLine("");
            }
        }
        public void Piramidetres()
        {
            int basePiramide = BasePiramide;
            for (int i = 0; i < AlturaPiramide; i++)
            {
                for (int j = 0; j < basePiramide; j++)
                {
                    Console.Write("3");
                }
                basePiramide--;
                Console.WriteLine("");
            }
        }
        public void Piramidecuatro()
        {
            int basePiramide = BasePiramide;
            for (int i = 0; i < AlturaPiramide; i++)
            {
                for (int j = 0; j < basePiramide; j++)
                {
                    Console.Write("4");
                }
                basePiramide--;
                Console.WriteLine("");
            }
        }
    }
}
