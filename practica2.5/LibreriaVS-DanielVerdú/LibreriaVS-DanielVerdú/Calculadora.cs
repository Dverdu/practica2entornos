﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibreriaVS_DanielVerdú
{
    public class Calculadora
    {
        private int NumeroUno;
        private int NumeroDos;


        public Calculadora(int numeroUno, int numeroDos)
        {
            NumeroUno = numeroUno;
            NumeroDos = numeroDos;

        }
        public int Suma()
        {
            int resultadoSuma;
            resultadoSuma =  this.NumeroUno + this.NumeroDos;
            return resultadoSuma;
        }
        public int Resta()
        {

            int resultadoResta;
            resultadoResta = this.NumeroUno - this.NumeroDos;
            return resultadoResta;
        }
        public int Multiplicar()
        {

            int resultadoMultiplicar;
            resultadoMultiplicar = this.NumeroUno * this.NumeroDos;
            return resultadoMultiplicar;
        }
        public int Dividir()
        {

            int resultadoDivision;
            resultadoDivision = this.NumeroUno / this.NumeroDos;
            return resultadoDivision;
        }
        public void ContarNumeros()
        {
            
            for (int i = NumeroUno; i < NumeroDos-1; i++)
            {
                Console.WriteLine(i);
            }
        }
    }
}
