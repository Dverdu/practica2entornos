package eclipseLibrerias;

import java.util.Scanner;

public class Librerias1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static int aleatorio(int fin) {
		return (int) Math.round((Math.random() * fin));
	}

	public static boolean esDivisor(int dividendo, int divisor) {
		boolean resultado = false;

		if (dividendo % divisor == 0) {
			resultado = true;
		}

		return resultado;
	}

	public static double calcularPrecioFinal(double precioBase, boolean ivaAplicado) {

		if (ivaAplicado) {
			return precioBase * 1.21;
		}

		return precioBase;
	}

	public static void mostrarTabla(int numero) {
		for (int i = 1; i <= 10; i++) {
			System.out.println(i + " x " + numero + " = " + (i * numero));
		}
	}

	public static int pedirEdad() {

		System.out.println("Dime tu edad");
		Scanner input = new Scanner(System.in);
		int edad = input.nextInt();

		input.close();

		return edad;

	}

}
