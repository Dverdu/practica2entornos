package eclipseLibrerias;

import java.util.Scanner;

public class Librerias2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static String leeCadenas() {
		String cadena = "";
		String cadenaLeida;
		Scanner input = new Scanner(System.in);

		do {
			System.out.println("Introduce una cadena");
			cadenaLeida = input.nextLine();

			cadena = cadena + cadenaLeida + ":";

		} while (!cadenaLeida.equals("fin"));

		input.close();

		return cadena;
	}

	public static int deCadenaANatural(String string) {
		int resultado = 0;
		int cifraInt;
		int potenciasDiez = 0;

		for (int i = string.length() - 1; i >= 0; i--) {

			cifraInt = string.charAt(i) - 48;

			resultado = resultado + (cifraInt * (int) Math.pow(10, potenciasDiez));
			potenciasDiez++;
		}
		return resultado;
	}

	public static int aleatorio(int inicio, int fin) {
		return (int) (Math.random() * (fin - inicio + 1)) + inicio;
	}

	public static String palabraAleatorio(int longitud) {

		String resultado = "";
		char caracter;

		for (int i = 0; i < longitud; i++) {
			caracter = (char) aleatorio(65, 90);
			resultado = resultado + caracter;
		}
		return resultado;
	}

	public static String cifrar(String cadena, int desfase) {
		String resultado = "";
		char caracter;
		int posicionCaracter;
		int desfaseRespectoaZ;

		for (int i = 0; i < cadena.length(); i++) {

			caracter = cadena.charAt(i);

			posicionCaracter = (int) caracter;
			posicionCaracter = posicionCaracter + desfase;
			caracter = (char) posicionCaracter;

			if (caracter > 'z') {
				desfaseRespectoaZ = caracter - 'z';
				posicionCaracter = 'a' + desfaseRespectoaZ - 1;
				caracter = (char) posicionCaracter;
			}

			resultado = resultado + caracter;
		}

		return resultado;
	}

}
