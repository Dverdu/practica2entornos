package es.daniel.ventana;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JSlider;
import java.awt.Color;
import javax.swing.JTable;

/**
 * ventana para gestionar el gimnasio
 * @author Daniel
 * @since 07/01/2018
 */
public class DanielVentana extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DanielVentana frame = new DanielVentana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DanielVentana() {
		setTitle("Gimnasio Danielaso");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("Archivo");
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Abrir");
		mnNewMenu.add(mntmNewMenuItem_1);

		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Reiniciar");
		mnNewMenu.add(mntmNewMenuItem_2);

		JMenuItem mntmNewMenuItem = new JMenuItem("Salir");
		mnNewMenu.add(mntmNewMenuItem);

		JMenu mnNewMenu_1 = new JMenu("Editar");
		menuBar.add(mnNewMenu_1);

		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Cortar");
		mnNewMenu_1.add(mntmNewMenuItem_3);

		JMenuItem mntmNewMenuItem_4 = new JMenuItem("Pegar");
		mnNewMenu_1.add(mntmNewMenuItem_4);

		JMenuItem mntmNewMenuItem_5 = new JMenuItem("Copiar");
		mnNewMenu_1.add(mntmNewMenuItem_5);

		JMenu mnNewMenu_2 = new JMenu("Ayuda");
		menuBar.add(mnNewMenu_2);

		JMenuItem mntmNewMenuItem_6 = new JMenuItem("Acerca de...");
		mnNewMenu_2.add(mntmNewMenuItem_6);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		panel.setBackground(Color.CYAN);
		tabbedPane.addTab("Alta", null, panel, null);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Nombre:");
		lblNewLabel.setBounds(3, 14, 63, 14);
		panel.add(lblNewLabel);

		JLabel lblApellidos = new JLabel("Apellidos:");
		lblApellidos.setBounds(3, 39, 63, 14);
		panel.add(lblApellidos);

		textField = new JTextField();
		textField.setBounds(66, 11, 86, 20);
		panel.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(66, 36, 86, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);

		JCheckBox chckbxMayorDeEdad = new JCheckBox("Mayor de edad");
		chckbxMayorDeEdad.setBounds(10, 63, 134, 23);
		panel.add(chckbxMayorDeEdad);

		JLabel lblTiempo = new JLabel("Tiempo:");
		lblTiempo.setBounds(10, 99, 46, 14);
		panel.add(lblTiempo);

		JComboBox comboBox = new JComboBox();
		comboBox.addItem("1 mes");
		comboBox.addItem("2 meses");
		comboBox.addItem("3 meses");
		comboBox.addItem("4 meses");
		comboBox.setBounds(66, 96, 86, 20);
		panel.add(comboBox);

		JButton btnNewButton = new JButton("Alta");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(3, 131, 89, 23);
		panel.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Reiniciar");
		btnNewButton_1.setBounds(102, 131, 89, 23);
		panel.add(btnNewButton_1);

		JRadioButton rdbtnNewRadioButton = new JRadioButton("Hombre");
		rdbtnNewRadioButton.setBounds(181, 10, 109, 23);
		panel.add(rdbtnNewRadioButton);

		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Mujer");
		rdbtnNewRadioButton_1.setBounds(181, 39, 109, 23);
		panel.add(rdbtnNewRadioButton_1);

		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnNewRadioButton);
		group.add(rdbtnNewRadioButton_1);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(255, 127, 80));
		tabbedPane.addTab("Consulta", null, panel_1, null);
		panel_1.setLayout(null);
		
		JLabel lblDatosAMostrar = new JLabel("Datos a mostrar:");
		lblDatosAMostrar.setBounds(10, 11, 117, 14);
		panel_1.add(lblDatosAMostrar);
		
		JSlider slider = new JSlider();
		slider.setBounds(129, 11, 200, 26);
		panel_1.add(slider);
		
		JSpinner spinner = new JSpinner();
		spinner.setValue(new Integer(10));
		spinner.setBounds(359, 8, 50, 20);
		panel_1.add(spinner);
		
		table = new JTable();
		table.setBounds(28, 60, 366, 118);
		panel_1.add(table);
	}
}
