﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaVS_DanielVerdú
{
    class Program
    {
        public static int resultado;
        static void Main(string[] args)
        {

            Menu();
        }
        public static int suma(int uno, int dos)
        {
            int suma;
            suma = uno + dos;
            return suma;
        }
        public static int dividir(int uno, int dos)
        {
             int dividir;
            dividir = uno / dos;
            return dividir;
        }
        public static int multiplicar(int uno, int dos)
        {
            int multiplicar;
            multiplicar = uno * dos;
            return multiplicar;
        }
        public static int resta(int uno, int dos)
        {
            int resta;
            resta = uno - dos;
            return resta;
        }
        public static void Menu()
        {
           
            Console.WriteLine("Pulsa un numero del 1 al 4 para elegir una opcion");
            Console.WriteLine("1: Sumar numeros");
            Console.WriteLine("2: Restar numeros");
            Console.WriteLine("3: Multiplicar numeros");
            Console.WriteLine("4: Dividir numeros");
            String opcionSeleccionada = Console.ReadLine();
            int numeroUno = PedirNumero();
            int numeroDos = PedirNumero();
            switch (opcionSeleccionada)
            {
                case "1":
                    resultado = suma(numeroUno,numeroDos);
                    break;
                case "2":
                    resultado = resta(numeroUno, numeroDos);
                    break;
                case "3":
                    resultado = multiplicar(numeroUno, numeroDos);
                    break;
                case "4":
                    resultado = dividir(numeroUno, numeroDos);
                    break;
                default:
                    break;
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("El resultado es:" + resultado);
            Menu();
        }
        public static int PedirNumero()
        {
            Console.WriteLine("Dame un Numero;");
            int segundonumero;
            segundonumero = Convert.ToInt32(Console.ReadLine());

            return segundonumero;
        }
    }
    
}
